import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import slick.driver.H2Driver.api._

import scala.util.Random

object HelloSlick extends App {

  val db = Database.forConfig("h2mem1")
  try {
    Await.result(db.run(DBIO.seq(
      Tables.users.schema.create,
      Tables.books.schema.create,

      Tables.users += User("John Doe"),
      Tables.users += User("Paul Smith"),

      Tables.books += Book("Book title 1", Random.nextDouble(), 1),
      Tables.books += Book("Book title 2", Random.nextDouble(), 1),
      Tables.books += Book("Book title 3", Random.nextDouble(), 2),
      Tables.books += Book("Book title 4", Random.nextDouble(), 2),

      Tables.users.result.map(println),
      Tables.books.result.map(println),

      Tables.users.joinLeft(Tables.books).on({
        case (user, book) => user.id === book.userId
      }).result.map(println)/*,

      Tables.users.map(user =>
        (user, None: Option[Book])
      ).result.map(println)*/

    )), Duration.Inf)
  } finally db.close
}

object Tables {
  val users = TableQuery[Users]
  val books = TableQuery[Books]
}

case class User(name: String, id: Option[Int] = None)
case class Book(name: String, price: Double, userId: Int, id: Option[Int] = None)

class Users(tag: Tag) extends Table[User](tag, "USERS") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def name = column[String]("NAME")

  def * = (name, id.?) <> (User.tupled, User.unapply)
}

class Books(tag: Tag) extends Table[Book](tag, "BOOKS") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def name = column[String]("NAME")
  def price = column[Double]("PRICE")
  def userId = column[Int]("USER_ID")

  foreignKey("USER_ID_FK", userId, Tables.users)(_.id)

  def * = (name, price, userId, id.?) <> (Book.tupled, Book.unapply)
}